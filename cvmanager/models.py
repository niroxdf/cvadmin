from django.db import models


# Create your models here.
class Curriculum(models.Model):
    link = models.CharField('cv link', max_length=200)
    target_tittle = models.CharField('target tittle', max_length=50)

    def __str__(self):
        return self.target_tittle + " : " + self.link


class Person(models.Model):
    curriculum = models.ForeignKey(Curriculum, on_delete=models.CASCADE)
    name = models.CharField(max_length=25)
    lastName = models.CharField(max_length=25)
    age = models.IntegerField(default=0)
    lastJobTittle = models.CharField('Last job tittle', max_length=50)
    lastJobCompany = models.CharField('Last job companny', max_length=50)
    lastJobDate = models.DateTimeField('Date of the last job')
    lastStudyTittle = models.CharField('Upper/Last academic tittle', max_length=50)
    lastStudyDate = models.DateTimeField('Date of the upper/last academic tittle')

    def __str__(self):
        return self.name + "_" + self.lastName

    def get_academic_info(self):
        return self.lastStudyTittle, self.lastStudyDate
