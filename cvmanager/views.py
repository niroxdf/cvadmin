from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader

from cvmanager.models import Person, Curriculum


def index(request):
    return render(request, 'cvmanager/index.html')


def person_info(request, person_id):
    person = Person.objects.filter(id=person_id)[0]
    response = "person: %s"
    output = person.name + " " + person.lastName
    return HttpResponse(response % output)


def curriculum_info(request, curriculum_id):
    response = "Curriculum : %s"
    return HttpResponse(response % curriculum_id)


def get_the_people(request):
    people = Person.objects.all()
    template = loader.get_template('cvmanager/people.html')
    context = {
        'people': people
    }
    output = ', '.join([p.name + " " + p.lastName + ": " + p.lastJobTittle for p in people])
    return HttpResponse(template.render(context, request))


def get_all_curriculum(request):
    curriculums = Curriculum.objects.all()
    template = loader.get_template('cvmanager/curriculums.html')
    context = {'curriculums': curriculums}
    return HttpResponse(template.render(context, request))
