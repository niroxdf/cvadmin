from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('people/<int:person_id>/', views.person_info, name='person_info'),
    path('curriculum/<int:curriculum_id>/', views.curriculum_info, name='curriculum_info'),
    path('people/', views.get_the_people, name='get_the_people'),
    path('curriculums/', views.get_all_curriculum, name='get all curriculums'),
]

