from django.contrib import admin
from .models import Curriculum, Person
# Register your models here.
admin.site.register(Curriculum)
admin.site.register(Person)